import React, { Component } from 'react';
import { Table,message,Pagination} from 'antd';
import  {userService}  from '../services/user-service';


class Users extends Component {
    constructor(props){
        super(props)
        this.state = { 
          users:{},
          limit:25
         }

         this.getUserData = this.getUserData.bind(this)
        

    }
      
  componentDidMount(){
    this.getUserData()
  }
   async getUserData(pageIndex){
     const mId= message.loading('Getting data Hold on..',0);
     let result = await new userService().getUsers(pageIndex);
     this.setState({users:result})
      setTimeout(mId,500);
   }
    

   onChange = page => {
    console.log(page);
    this.getUserData(page)
  };


    render() { 
      const columns = [
        {
          title: 'Frist Name',
          dataIndex: 'firstName',
          key: 'first_name',
        },
        {
          title: 'Last Name',
          dataIndex: 'lastName',
          key: 'last_name',
        },
        {
          title: 'Video Count',
          dataIndex: 'videoCount',
          key: 'video_count',
        },
      ];
        return(
            <React.Fragment>
            <h1 style={{textAlign:"center"}}>Users Browser</h1> 
            <Table pagination={false} dataSource={this.state.users.result} columns={columns}></Table>
            <Pagination style={{marginTop:15,float:"right"}} onChange={this.onChange} defaultCurrent={this.state.page} pageSize={this.state.limit} total={this.state.users.total} />
            </React.Fragment>
        )
       
    }
    
}

 
export default Users;